# PSCDarkening Project ERIS Theme for PlayStation Classic

**By SMR9000**

```
PLEASE NOTE: This theme is user submitted and not endorsed or created by ModMyClassic. All material should be free to use with third party agreement to allow the use of any third party materials. If you believe there is a conflict of interest, please contact us directly on our discord https://modmyclassic.com/discord
```

## How to use

### Project ERIS (PlayStation Classic)
1. Download the .mod package from [the Project ERIS mods page](https://modmyclassic.com/project-eris-mods/)
2. Copy the .mod package to your USB to: `USB:/project_eris/mods/`
3. Plug in and boot up the PlayStation Classic. Project ERIS will automatically install.
4. Select your custom theme "SMR-PSCDarkening" from the options menu at the boot menu

**Note: This theme replaces the original bootmusic, Backup of original music is included with theme
"project_eris\etc\project_eris\SND\originalsong_backup" if you want to restore it.

## Credits

**Music Credits:**
WhiteBat Audio: "Night-Dweller"

**Art Credits**
HAL9000 aka SMR9000

## Preview

![preview](https://cdn.discordapp.com/attachments/692699532651134993/709894185183871057/Background.png)